# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="6"
K_NOUSENAME="yes"
K_NOSETEXTRAVERSION="yes"
K_SECURITY_UNSUPPORTED="1"
ETYPE="sources"
inherit kernel-2 git-r3 eapi7-ver
detect_version

DESCRIPTION="A free (as in freedom) variant of the Linux kernel"
HOMEPAGE="https://www.fsfla.org/ikiwiki/selibre/linux-libre/"
EGIT_REPO_URI="https://jxself.org/git/linux-libre.git"
EGIT_COMMIT="v${PVR}-gnu"
EGIT_CLONE_TYPE="shallow"
EGIT_CHECKOUT_DIR="${WORKDIR}/linux-libre-${PVR}";
S="${WORKDIR}/linux-libre-${PVR}"

KEYWORDS="alpha amd64 arm arm64 hppa ia64 mips ppc ppc64 s390 sparc x86"
